// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import BootstrapVue from "bootstrap-vue";
import apiService from "./services/apiservice.js";
import Viewer from "v-viewer";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import { store } from "./store/store.js";
import { swal } from "sweetalert";
Vue.config.productionTip = false;
Vue.prototype.$apiservice = apiService;
Vue.use(BootstrapVue);
Vue.use(Viewer);

import VueCarousel from "vue-carousel";
Vue.use(VueCarousel);

Vue.prototype.$eventHub = new Vue();
/* eslint-disable no-new */

new Vue({
  el: "#app",
  router,
  store: store,
  swal: swal,
  render: h => h(App)
});
