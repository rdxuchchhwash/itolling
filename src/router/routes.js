import { store } from "../store/store.js";

function load(component) {
  // '@' is aliased to src/components
  return () =>
    import(/* webpackChunkName: "[request]" */ `@/pages/${component}.vue`);
}

async function accessValidation(to, from, next) {
  //console.log("route hook to.name=> ", to.name, "is logged in ", store.getters.getLoggedInStatus);
  if (to.name !== "signin" && !store.getters.getLoggedInStatus) {
    next("/");
  } else {
    next();
  }
}

export default [
  {
    path: "/",
    name: "signin",
    component: load("Signin"),
    beforeEnter: accessValidation
  },
  {
    path: "/dashboard",
    name: "dashboard",
    component: load("Dashboard"),
    beforeEnter: accessValidation
  },
  {
    path: "/listuser",
    name: "listuser",
    component: load("ListUser"),
    beforeEnter: accessValidation
  },
  {
    path: "/change-password",
    name: "change-password",
    component: load("ChangePassword"),
    beforeEnter: accessValidation
  },
  {
    path: "*",
    component: load("404")
  }
];
