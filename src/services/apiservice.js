import Axios from "axios";
import { store } from "../store/store.js";
import router from "../router";

var baseURL =
  window.location.host.slice(0, 9) === "localhost"
    ? "http://localhost:3000"
    : "https://LIVE_URL_PATH";
var baseURL = "https://irapi.latticeit.co.za";

const axiosInstance = Axios.create({
  //withCredentials:  true,
  headers: {
    //'Access-Control-Allow-Origin': '*',
    "Content-Type": "application/json"
    //'access_token': "fKngSLsiNEJjIGUMOCksVvBIeNnxGSlNNKchCNzQDprDbdgubuA4Aadmin"
  }
});

axiosInstance.interceptors.request.use(
  config => {
    let loginStatus = store.getters.getLoggedInStatus;

    if (loginStatus) {
      config.headers["Access-Control-Allow-Origin"] = "*";
      config.headers["access_token"] = `${store.getters.getUser.access_token}`;
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  response => response,
  error => {
    const status = error.response ? error.response.status : null;
    if (status === 401) {
      swal({
        title: "Access token expired",
        text: "Please login again",
        icon: "error",
        type: "error",
        button: false,
        timer: 1500
      }).then(function() {});
      store.commit("setLoginStatus", false);
      router.push("/");
      //return axiosInstance.request(error.config);
    }
    return Promise.reject(error);
  }
);

export default {
  reqApi(url) {
    return {
      post: data => axiosInstance.post(baseURL + url, data),
      getByBody: data => axiosInstance.get(baseURL + url, data),
      put: data => axiosInstance.put(baseURL + url, data),
      update: data => axiosInstance.put(baseURL + url, data),
      get: () => axiosInstance.get(baseURL + url),
      getById: id => axiosInstance.get(`${baseURL + url}/${id}`),
      delete: id => axiosInstance.delete(`${baseURL + url}/${id}`),
      putByid: id => axiosInstance.put(`${baseURL + url}/${id}`),
      postById: id => axiosInstance.post(`${baseURL + url}/${id}`),
      email: toMail => axiosInstance.post(baseURL + url, toMail)
    };
  },

  remote(url) {
    return {
      create: toCreate => axiosInstance.post(url, toCreate),
      update: toUpdate => axiosInstance.put(url, toUpdate),
      get: () => axiosInstance.get(url),
      delete: () => axiosInstance.delete(url),
      email: toMail => axiosInstance.post(url, toMail)
    };
  }
};
